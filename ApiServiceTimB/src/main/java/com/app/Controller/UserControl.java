package com.app.Controller;

import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.app.Model.userModel;
import com.app.Repo.BiodataRepo;
import com.app.Repo.userRepo;

@RestController
@RequestMapping("api/user")
@CrossOrigin(origins = "*")
public class UserControl {

	@Autowired
	private userRepo ur;

	@Autowired
	private BiodataRepo br;

	@GetMapping("list")
	public List<userModel> userorder() {
		return ur.userorder();
	}

	@GetMapping("list1")
	public List<Map<String, Object>> list_User() {
		return ur.list_User();
	}
	
	@PutMapping("edit")
	public void editLoginAttempt(@RequestBody userModel um) {
		ur.save(um);
	}

	@PostMapping("post")
	public void post(@RequestBody userModel um) {
		um.setIslocked(false);
		um.setLastlogin(LocalDateTime.now());
		um.setCreated_on(LocalDateTime.now());
		um.setCreated_by(0);
		ur.save(um);
		long nil = ur.selectById();
		ur.setDefaultCreatedBy(nil);
	}

	// cek email
	@GetMapping("cekemail/{email}")
	public int cekEmail(@PathVariable String email) {
		return ur.cek_Email(email);
	}
	@GetMapping("list/{id}")
	public userModel userById(@PathVariable long id){
		return ur.userbyid(id);
	}
	
	// cek email + password
	@GetMapping("ceklogin/{em}/{ps}")
	public Map<String, Object> ceklogin(@PathVariable String em, @PathVariable String ps) {
		return ur.ceklogin(em, ps);
	}

	@GetMapping("getrole/{id}")
	public List<Map<String, Object>> getrole(@PathVariable long id){
		return ur.getrole(id);
	}
	/*
	 * @PostMapping("setsession") public String
	 * setsession(@RequestParam(value=email") String un,"
	 * +"@RequestParam(value= "password") String ps, " + "HttpServletRequest
	 * request) { System.out.println(un+ps); //username harus ikan pass 123
	 * 
	 * if(un.equals("") && ps.equals("123")) {
	 * request.getSession().setAttribute("userlogin","ikan"); return "redirect:/";
	 * }else { return "redirect:/login"; }
	 * 
	 * }
	 */
	 
	  @GetMapping("ceksession") 
	  public String cekSesion(HttpSession session ) {
		  //cek sesion variabel dengan nama user login Object userlogin=
		  session.getAttribute("userlogin"); //handle object to string
	  	  return "redirect:/"; 
	  }
	 

}
