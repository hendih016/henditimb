package com.app.Controller;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.app.Repo.DoctorRepo;

@RestController
@RequestMapping("api/doctor")
@CrossOrigin(origins = "*")
public class DoctorControl {

	@Autowired
	private DoctorRepo dr;

//	@GetMapping("list")
//	public List<Map<Long, Object>> listDoctor() {
//		return dr.listDoctor();
//	}

	@GetMapping("poplocs")
	public List<Map<Long, Object>> popLocs() {
		return dr.popLocs();
	}

	@GetMapping("popname")
	public List<Map<Long, Object>> popName() {
		return dr.popName();
	}

	@GetMapping("popspec")
	public List<Map<Long, Object>> popSpec() {
		return dr.popSpec();
	}

	@GetMapping("popmedt")
	public List<Map<Long, Object>> popMedt() {
		return dr.popMedt();
	}
	
	@GetMapping("popmedt/{spec}")
	public List<Map<Long, Object>> popMedtBySpec(@PathVariable Long spec) {
		return dr.popMedtBySpec(spec);
	}
	
	@GetMapping("listdoctor")
	public List<Map<Long, Object>> listBySpec(@RequestParam Long specId, @RequestParam String name) {
		return dr.listDoctor(specId,name);
	}
	
	
//	@GetMapping("listbyname/{name}")
//	public List<Map<Long, Object>> listByName(@PathVariable Long string) {
//		return dr.listBySpec(spec);
//	}
	

}
