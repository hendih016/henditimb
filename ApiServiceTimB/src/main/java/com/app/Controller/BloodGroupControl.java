package com.app.Controller;

import java.time.LocalDateTime;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.app.Model.BloodGroupModel;
import com.app.Model.CustomerRelationModel;
import com.app.Repo.BloodGroupRepositorys;
import com.app.Repo.CustomerRelationRepositorys;


@RestController
@RequestMapping("api/blood")
@CrossOrigin(origins = "*")
public class BloodGroupControl {
	
	@Autowired
	private CustomerRelationRepositorys crr;
	
	@GetMapping("list")
	public List<CustomerRelationModel> listCsRelation(){
		return crr.listCsRelation();
	}
	
	@PostMapping("addcr")
	public void addcr(@RequestBody CustomerRelationModel crm) {
		LocalDateTime today = LocalDateTime.now();
		crm.setCreatedBy(1);
		crm.setCreatedOn(today);
		crr.save(crm);
		
	}
	
	@PutMapping("editcr")
	public void editcr(@RequestBody CustomerRelationModel crm) {
		LocalDateTime today = LocalDateTime.now();
		crm.setModifedBy(1);
		crm.setModifedOn(today);
		crr.save(crm);
	}
	
	@DeleteMapping("deletecr")
	public void deletecr(@RequestBody CustomerRelationModel crm) {
		LocalDateTime today = LocalDateTime.now();
		crm.setDeletedBy(1);
		crm.setDeletedOn(today);
		crr.deleteCsRelation(crm.getId());
	}
	
	@GetMapping("listbyname/{name}")
	public List<CustomerRelationModel> listbyname(@PathVariable String name){
		return crr.listCsRelationbyNm(name);
	}
	
	@GetMapping("list/{id}")
	public CustomerRelationModel Crbyid(@PathVariable long id) {
		return crr.CsRelationbyId(id);
	}
}
