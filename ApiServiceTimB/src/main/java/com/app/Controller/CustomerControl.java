package com.app.Controller;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.app.Model.BiodataModel;
import com.app.Model.CustomerMemberModel;
import com.app.Model.CustomerModel;
import com.app.Model.PojoTambahPasienModel;
import com.app.Repo.BiodataRepo;
import com.app.Repo.CustomerMemberRepo;
import com.app.Repo.CustomerRepo;

@RestController
@RequestMapping("api/tambahpasien")
@CrossOrigin(origins = "*")
public class CustomerControl {
	
	
	@Autowired
	private BiodataRepo br;
	
	@Autowired
	private CustomerRepo cr;
	
	@Autowired
	private CustomerMemberRepo cmr;
	
	
	@PostMapping("add")
	public void add(@RequestBody PojoTambahPasienModel ppm) {
		System.out.println(ppm.getFullname());
		
		//save fullname biodata
		BiodataModel bm = new BiodataModel();
		bm.setFullname(ppm.getFullname());
		br.save(bm);
		
		//save customer
		CustomerModel cm= new CustomerModel();
		cm.setBiodata_id(bm.getId());
		cm.setDob(ppm.getDob());
		cm.setGender(ppm.getGender());  // gender
		cm.setBlood_group_id(ppm.getBlood_group_id());  // goldar
		cm.setRhesus_type(ppm.getRhesus_type()); // rhesus darah
		cm.setHeight(ppm.getHeight()); // tinggi
		cm.setWeight(ppm.getWeight()); // berat
		cr.save(cm);
		
		
		// save customer member model
		CustomerMemberModel cmm = new CustomerMemberModel();
		cmm.setParent_biodata_id(bm.getId());
		cmm.setCustomer_id(cm.getId());
		cmm.setCustomer_relation_id(ppm.customer_relation_id);
		cmr.save(cmm);
	}
	
	@GetMapping("list")
	public List<Map<String, Object>>listCS(){
		return cr.listCS();
	}
	
	@GetMapping("list/{id}")
	public List<Map<String, Object>> listbyid(@PathVariable long id){
		return cr.listCSById(id);
	}
}
