package com.app.Controller;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.app.Model.BiodataModel;
import com.app.Repo.ProfileRepo;

@RestController
@RequestMapping("api/tabprofile")
@CrossOrigin(origins = "*")
public class TabProfileController {

	@Autowired
	private ProfileRepo pr;
	
	@GetMapping("listprofile/{id}")
	public List<Map<String, Object>> listprofile(@PathVariable long id){
		return pr.listprofile(id);
	}
	
	@GetMapping("listprofiletahun/{id}")
	public BiodataModel listprofilethn(@PathVariable long id){
		return pr.listprofiletahun(id);
	}
	
	@GetMapping("list2")
	public List<BiodataModel> listprofil2(){
		return pr.listprofile2();
	}

}
