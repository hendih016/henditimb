package com.app.Repo;

import java.util.List;
import java.util.Map;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import com.app.Model.LevelLokasiModel;

@Transactional
public interface LevelLokasiRepo extends JpaRepository<LevelLokasiModel, Long> {

	@Query(value = "select * from m_location_level where is_delete = false", 
			nativeQuery = true)
	List<LevelLokasiModel> listLevelLokasi();

	@Query(value = "SELECT * FROM m_location_level WHERE LOWER(name) LIKE LOWER(CONCAT('%',:name,'%'))", 
			nativeQuery = true)
	List<LevelLokasiModel> listLevLokByName(String name);

	@Query(value = "SELECT COUNT(id) FROM m_location WHERE location_level_id = :id", 
			nativeQuery = true)
	int countLokById(Long id);

	@Modifying
	@Query(value = "update m_location_level set is_delete=true where id= :id ",nativeQuery = true)
	int deleteLocLev(long id);

	@Query(value = "SELECT * FROM m_location_level WHERE id = :id", 
			nativeQuery = true)
	LevelLokasiModel LevLokById(Long id);
	
	@Modifying
	@Query(value = "INSERT INTO m_location_level (id, Name, Abbreviation, Created_by, Created_on) VALUES\r\n"
			+ "	(:id, :name, :abbr, :C_by, NOW())",nativeQuery = true)
	int addLevLok(Long id, String name, String abbr, Long C_by);
	
	@Query(value = "SELECT name FROM m_location_level WHERE LOWER(name) LIKE LOWER(CONCAT('%',:name,'%')) AND is_delete = false LIMIT 1", 
			nativeQuery = true)
	String cekDupLocLev(String name);
	
	@Query(value = "Select max(id) from m_location_level", 
			nativeQuery = true)
	Long getLastId();
}
