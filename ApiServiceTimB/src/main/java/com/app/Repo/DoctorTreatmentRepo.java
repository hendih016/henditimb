package com.app.Repo;

import java.util.List;
import java.util.Map;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.app.Model.DoctorTreatmentModel;

@Transactional
public interface DoctorTreatmentRepo extends JpaRepository<DoctorTreatmentModel, Long> {
	
}
