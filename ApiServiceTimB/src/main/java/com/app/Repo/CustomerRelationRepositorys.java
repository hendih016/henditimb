package com.app.Repo;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import com.app.Model.CustomerRelationModel;
@Transactional
public interface CustomerRelationRepositorys  extends JpaRepository<CustomerRelationModel, Long>{


	@Query(value = "select * from m_customer_relation where is_delete = false order by id",nativeQuery = true)
	List<CustomerRelationModel> listCsRelation();
	
	@Modifying
	@Query(value = "update m_customer_relation set is_delete= true where id=:id" ,nativeQuery = true)
	int deleteCsRelation(long id);
	
	@Query(value = "select * from m_customer_relation where is_delete= false and lower(name) like lower(concat('%',:nm,'%'))",nativeQuery = true)
	List<CustomerRelationModel> listCsRelationbyNm(String nm);
	
	@Query(value = "select *from m_customer_relation where id=:id limit 1",nativeQuery = true)
	CustomerRelationModel CsRelationbyId(long id);
}
