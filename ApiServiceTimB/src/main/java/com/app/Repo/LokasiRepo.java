package com.app.Repo;

import java.util.List;
import java.util.Map;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import com.app.Model.LevelLokasiModel;
import com.app.Model.LokasiModel;

@Transactional
public interface LokasiRepo extends JpaRepository<LokasiModel, Long> {

	@Query(value = "SELECT ML.ID, MLL.name levelname,\r\n"
			+ "	CONCAT(MLL.ABBREVIATION,' ',ML.NAME) AS namadaerah,\r\n"
			+ "	COALESCE((SELECT \r\n"
			+ "	 CONCAT(MLL1.ABBREVIATION,' ',\r\n"
			+ "			ML1.NAME)\r\n"
			+ "		FROM M_LOCATION ML1\r\n"
			+ "		JOIN M_LOCATION_LEVEL MLL1 ON ML1.LOCATION_LEVEL_ID = MLL1.ID\r\n"
			+ "		WHERE ML1.ID = ML.PARENT_ID),'') as namadaerahatas\r\n"
			+ "FROM M_LOCATION ML\r\n"
			+ "JOIN M_LOCATION_LEVEL MLL ON ML.LOCATION_LEVEL_ID = MLL.ID\r\n"
			+ "WHERE ML.IS_DELETE = FALSE", nativeQuery = true)
	List<Map<Long, Object>> listLokasi();

	@Query(value = "SELECT ML.ID, MLL.name levelname,\r\n"
			+ "	CONCAT(MLL.ABBREVIATION,' ',ML.NAME) AS namadaerah,\r\n"
			+ "	COALESCE((SELECT \r\n"
			+ "	 CONCAT(MLL1.ABBREVIATION,' ',\r\n"
			+ "			ML1.NAME)\r\n"
			+ "		FROM M_LOCATION ML1\r\n"
			+ "		JOIN M_LOCATION_LEVEL MLL1 ON ML1.LOCATION_LEVEL_ID = MLL1.ID\r\n"
			+ "		WHERE ML1.ID = ML.PARENT_ID),'Kosong') as namadaerahatas\r\n"
			+ "FROM M_LOCATION ML\r\n"
			+ "JOIN M_LOCATION_LEVEL MLL ON ML.LOCATION_LEVEL_ID = MLL.ID\r\n"
			+ "WHERE ML.IS_DELETE = FALSE AND ML.ID = :id", nativeQuery = true)
	List<Map<Long, Object>> listLokasiById(Long id);

	@Query(value = "SELECT ML.ID, MLL.name levelname,\r\n"
			+ "	CONCAT(MLL.ABBREVIATION,' ',ML.NAME) AS namadaerah,\r\n"
			+ "	COALESCE((SELECT \r\n"
			+ "	 CONCAT(MLL1.ABBREVIATION,' ',\r\n"
			+ "			ML1.NAME)\r\n"
			+ "		FROM M_LOCATION ML1\r\n"
			+ "		JOIN M_LOCATION_LEVEL MLL1 ON ML1.LOCATION_LEVEL_ID = MLL1.ID\r\n"
			+ "		WHERE ML1.ID = ML.PARENT_ID),'Kosong') as namadaerahatas\r\n"
			+ "FROM M_LOCATION ML\r\n"
			+ "JOIN M_LOCATION_LEVEL MLL ON ML.LOCATION_LEVEL_ID = MLL.ID\r\n"
			+ "WHERE ML.IS_DELETE = FALSE AND LOWER(ML.NAME) LIKE LOWER(CONCAT('%',:name,'%'))", nativeQuery = true)
	List<Map<Long, Object>> listLokByName(String name);

	@Modifying
	@Query(value = "update m_location set is_delete=true where id= :id ", nativeQuery = true)
	int deleteLok(long id);

	@Modifying
	@Query(value = "INSERT INTO m_location (id, name, location_level_id, parent_id, created_by, created_on) VALUES\r\n"
			+ "	(:id, :name, :loclevid, :pId, :C_by, NOW())",nativeQuery = true)
	int addLevLok(Long id, String name, Long loclevid, Long pId, Long C_by);
}
