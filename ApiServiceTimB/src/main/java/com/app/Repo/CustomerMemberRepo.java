package com.app.Repo;

import org.springframework.data.jpa.repository.JpaRepository;

import com.app.Model.CustomerMemberModel;

public interface CustomerMemberRepo extends JpaRepository<CustomerMemberModel, Long>{

}
