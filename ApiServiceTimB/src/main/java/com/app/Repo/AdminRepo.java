package com.app.Repo;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import com.app.Model.AdminModel;


@Transactional
public interface AdminRepo extends JpaRepository<AdminModel, Long> {

	@Query(value = "select * from m_admin where is_delete=false order by id", nativeQuery = true)
	List<AdminModel> listadmin();
	
	@Query(value = "select * from m_admin where id= :id limit 1", nativeQuery = true)
	AdminModel adminbyid(long id);
	
	@Modifying
	@Query(value = "update m_admin set is_delete=true where id= :id ", nativeQuery = true)
	int deleteadmin(long id);
}
