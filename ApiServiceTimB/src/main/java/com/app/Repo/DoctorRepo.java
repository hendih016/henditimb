package com.app.Repo;

import java.util.List;
import java.util.Map;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.app.Model.DoctorModel;

@Transactional
public interface DoctorRepo extends JpaRepository<DoctorModel, Long> {

	@Query(value = "select l.id, concat(ll.name,' ',l.name) namalokasi from m_location l\r\n"
			+ "join m_location_level ll ON ll.id = l.location_level_id\r\n", nativeQuery = true)
	List<Map<Long, Object>> popLocs();

	@Query(value = "select b.id, b.fullname from m_doctor d \r\n"
			+ "join m_biodata b ON b.id = d.biodata_id", nativeQuery = true)
	List<Map<Long, Object>> popName();

	@Query(value = "select distinct(s.name), s.id specialisasi from m_doctor d\r\n"
			+ "join t_current_doctor_specialization cds on cds.doctor_id = d.id\r\n"
			+ "join m_specialization s on s.id = cds.specialization_id\r\n", nativeQuery = true)
	List<Map<Long, Object>> popSpec();

	@Query(value = "SELECT distinct(dt.name), d.id from m_doctor d\r\n"
			+ "join t_doctor_treatment dt on d.id = dt.doctor_id", nativeQuery = true)
	List<Map<Long, Object>> popMedt();

	@Query(value = "SELECT distinct(dt.name), cds.specialization_id from m_doctor d\r\n"
			+ "	join t_doctor_treatment dt on d.id = dt.doctor_id \r\n"
			+ "	join t_current_doctor_specialization cds on cds.doctor_id = d.id\r\n"
			+ "	WHERE cds.specialization_id = :spec\r\n", nativeQuery = true)
	List<Map<Long, Object>> popMedtBySpec(Long spec);

//	@Query(value = "SELECT d.id as id, b.fullname, s.name as specname, mf.name as officename, (\r\n"
//			+ "Select EXTRACT(YEAR FROM now())-TO_NUMBER(end_year,'9999') as tahun from m_doctor_education WHERE id=D.ID AND is_last_education = true\r\n"
//			+ ") FROM m_doctor d\r\n" + "JOIN m_biodata b ON d.biodata_id = b.id\r\n"
//			+ "JOIN m_doctor_education de ON d.id = de.doctor_id\r\n"
//			+ "JOIN t_current_doctor_specialization cds ON d.id = cds.doctor_id\r\n"
//			+ "JOIN m_specialization s ON cds.specialization_id = s.id\r\n"
//			+ "JOIN t_doctor_office dof ON d.id = dof.doctor_id\r\n"
//			+ "JOIN m_medical_facility mf ON dof.medical_facility_id = mf.id", nativeQuery = true)
//	List<Map<Long, Object>> listDoctor();
	
	@Query(value = "With tName AS(\r\n"
			+ "WITH tSpec AS(\r\n"
			+ "SELECT d.id as id, b.fullname, s.name as namaspesialis, (\r\n"
			+ "SELECT EXTRACT(YEAR FROM now())-TO_NUMBER(end_year,'9999') as tahun from m_doctor_education WHERE id=D.ID AND is_last_education = true\r\n"
			+ "), b.image, b.image_path, dt.name, dt.id medtId FROM m_doctor d\r\n"
			+ "JOIN m_biodata b ON d.biodata_id = b.id\r\n"
			+ "JOIN m_doctor_education de ON d.id = de.doctor_id\r\n"
			+ "JOIN t_current_doctor_specialization cds ON d.id = cds.doctor_id\r\n"
			+ "JOIN m_specialization s ON cds.specialization_id = s.id\r\n"
			+ "JOIN t_doctor_office dof ON d.id = dof.doctor_id\r\n"
			+ "JOIN t_doctor_office_treatment dot ON dot.doctor_office_id = dof.id\r\n"
			+ "JOIN t_doctor_treatment dt ON dt.id = dot.doctor_treatment_id\r\n"
			+ "WHERE s.id = :specId\r\n"
			+ ")\r\n"
			+ "SELECT * FROM tSpec ts WHERE LOWER(ts.fullname) LIKE LOWER(CONCAT('%',:name,'%'))\r\n", nativeQuery = true)
	List<Map<Long, Object>> listDoctor(Long specId, String name);
	
	
	//List<Map<Long, Object>> cariDokter(Long idLoc, String nmDoc, Long idSpe, Long idMdt);
}
