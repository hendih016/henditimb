package com.app.Repo;

import java.util.List;
import java.util.Map;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.app.Model.CustomerModel;

public interface CustomerRepo extends JpaRepository<CustomerModel, Long> {
	
	@Query(value ="select"
            + "    c.id,"
            + "    b.fullname,"
            + "    c.gender,"
            + "    bg.code,"
            + "    date_part('year', age(date(c.dob))) as usia,"
            + "    c.rhesus_type,"
            + "    c.height,"
            + "    c.weight,"
            + "    cr.name"
            + " from m_biodata b "
            + "join m_customer c on b.id = c.biodata_id "
            + "join bloodgroup bg on c.blood_group_id = bg.id "
            + "join m_customer_member cm on c.id = cm.customer_id "
            + "join m_customer_relation cr on cr.id = cm.customer_relation_id "
            + "order by c.id", nativeQuery = true)
		List<Map<String, Object>> listCS();
	
	@Query(value ="select"
            + "    c.id,"
            + "    b.fullname,"
            + "    c.gender,"
            + "    bg.code,"
            + " to_char(c.dob, 'DD-MM-YYYY') as tanggalLahir,"
            + "    date_part('year', age(date(c.dob))) as usia,"
            + "    c.rhesus_type,"
            + "    c.height,"
            + "    c.weight,"
            + "    cr.name"
            + " from m_biodata b "
            + "join m_customer c on b.id = c.biodata_id "
            + "join bloodgroup bg on c.blood_group_id = bg.id "
            + "join m_customer_member cm on c.id = cm.customer_id "
            + "join m_customer_relation cr on cr.id = cm.customer_relation_id "
            + "where c.id = :id", nativeQuery = true)
    List<Map<String, Object>> listCSById(long id);
}
