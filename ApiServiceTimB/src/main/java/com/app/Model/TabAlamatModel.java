package com.app.Model;

import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "mBiodataAddress")
public class TabAlamatModel {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private long id;
	
	@Column(name = "biodataId")
	private long biodataid;
	
	@Column(name = "label", length = 100)
	private String label;
	
	@Column(name = "recipient", length = 100)
	private String recipient;
	
	@Column(name = "recipientPhoneNumber", length = 15)
	private String recipientphonenumber;
	
	@Column(name = "locationId")
	private long locationid;
	
	@Column(name = "postalCode")
	private String postalcode;
	
	@Column(name = "address")
	private String address;
	
	@Column(name = "createdBy")
	private long createdby;
	
	@Column(name = "createdOn")
	private LocalDateTime createdon;
	
	@Column(name = "modifiedBy")
	private long modifiedby;
	
	@Column(name = "modifiedOn")
	private LocalDateTime modifiedon;
	
	@Column(name = "deletedBy")
	private long deletedby;
	
	@Column(name = "deletedOn")
	private LocalDateTime deletedon;
	
	@Column(name = "isDelete", columnDefinition = "boolean default false")
	private boolean delete;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public long getBiodataid() {
		return biodataid;
	}

	public void setBiodataid(long biodataid) {
		this.biodataid = biodataid;
	}

	public String getLabel() {
		return label;
	}

	public void setLabel(String label) {
		this.label = label;
	}

	public String getRecipient() {
		return recipient;
	}

	public void setRecipient(String recipient) {
		this.recipient = recipient;
	}

	public String getRecipientphonenumber() {
		return recipientphonenumber;
	}

	public void setRecipientphonenumber(String recipientphonenumber) {
		this.recipientphonenumber = recipientphonenumber;
	}

	public long getLocationid() {
		return locationid;
	}

	public void setLocationid(long locationid) {
		this.locationid = locationid;
	}

	public String getPostalcode() {
		return postalcode;
	}

	public void setPostalcode(String postalcode) {
		this.postalcode = postalcode;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public long getCreatedby() {
		return createdby;
	}

	public void setCreatedby(long createdby) {
		this.createdby = createdby;
	}

	public LocalDateTime getCreatedon() {
		return createdon;
	}

	public void setCreatedon(LocalDateTime createdon) {
		this.createdon = createdon;
	}

	public long getModifiedby() {
		return modifiedby;
	}

	public void setModifiedby(long modifiedby) {
		this.modifiedby = modifiedby;
	}

	public LocalDateTime getModifiedon() {
		return modifiedon;
	}

	public void setModifiedon(LocalDateTime modifiedon) {
		this.modifiedon = modifiedon;
	}

	public long getDeletedby() {
		return deletedby;
	}

	public void setDeletedby(long deletedby) {
		this.deletedby = deletedby;
	}

	public LocalDateTime getDeletedon() {
		return deletedon;
	}

	public void setDeletedon(LocalDateTime deletedon) {
		this.deletedon = deletedon;
	}

	public boolean isDelete() {
		return delete;
	}

	public void setDelete(boolean delete) {
		this.delete = delete;
	}
	
}
