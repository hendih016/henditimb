package com.app.com;

import org.springframework.stereotype.Component;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;

@Component
public class MailConfig {

	@Autowired
	private JavaMailSender jms;
	
	public void kirim(String to, String subject, String body) {
		
		SimpleMailMessage pesan = new SimpleMailMessage();
		pesan.setTo(to);
		pesan.setSubject(subject);
		pesan.setText(body);
		jms.send(pesan);
	}
	

}
