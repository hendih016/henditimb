INSERT INTO m_location_level (Name, Abbreviation, Created_by, Created_on, Is_delete) VALUES
	('Provinsi', 'Prov.', 1, NOW(), False),
	('Kota', 'Kota', 1, NOW(), False),
	('Kabupaten', 'Kab.', 1, NOW(), False),
	('Kecamatan', 'Kec.', 1, NOW(), False),
	('Kelurahan', 'Kel.', 1, NOW(), False),
	('Desa', 'Desa', 1, NOW(), False);
	
INSERT INTO m_location (name, parent_id, location_level_id, Created_by, Created_on) VALUES
	('Jakarta Selatan', 4, 2, 1, NOW()),
	('Pasar Minggu', 1, 4, 1, NOW()),
	('Kebagusan', 2, 5, 1, NOW()),
	('Jawa Barat', 0, 1, 1, NOW()),
	('DKI Jakarta', 0, 1, 1, NOW());

INSERT INTO m_biodata (Fullname , Mobile_phone, Created_by, Created_on) VALUES
	('Agus Mulyana', '+628989898989', 1, NOW()),
	('Rahma Wati', '+628989898989', 1, NOW()),
	('Marissa Panggabean', '+628989898989', 1, NOW()),
	('santo','087566363',1,NOW());
	
INSERT INTO m_doctor (biodata_id, str, Created_by, Created_on) VALUES
	('1', 'Gatau isinya apa 1','1', 'NOW()'),
	('2', 'Gatau isinya apa 2','1', 'NOW()'),
	('3', 'Gatau isinya apa 3','1', 'NOW()');
	
INSERT INTO m_medical_facility (name, medical_facility_category_id, location_id, full_address, email, phone_code, phone, fax, Created_by, Created_on) VALUES
	('RS Mitra', '1', '1', 'Alamatnya dimana ya', 'rsmitra@gmail.com', '+021', '89787878', '+02189787878', '1', 'NOW()'),
	('RS Persahabatan', '2', '2', 'Alamatnya dimana ya', 'rspersahabata@gmail.com', '+021', '81818181', '+02189787878', '1', 'NOW()');
	
INSERT INTO m_specialization (name, Created_by, Created_on) VALUES
	('Spesialis Anak','1', 'NOW()'),
	('Umum', '1', 'NOW()');
	
INSERT INTO t_current_doctor_specialization (doctor_id, specialization_id, Created_by, Created_on) VALUES
	('1', '1', '1', 'NOW()'),
	('2', '1', '1', 'NOW()'),
	('3', '2', '1', 'NOW()');
	
INSERT INTO m_doctor_education (doctor_id, education_level_id, institution_name, major, start_year, end_year, is_last_education, Created_by, Created_on) VALUES
	('1', '1', 'Universitas A', 'Kedokteran A', '2014', '2020', 'true', '1', 'NOW()'),
	('2', '2', 'Universitas B', 'Kedokteran B', '2013', '2019', 'true', '1', 'NOW()'),
	('3', '1', 'Universitas A', 'Kedokteran A', '1995', '2002', 'true', '1', 'NOW()');

INSERT INTO t_doctor_office (doctor_id, medical_facility_id, Created_by, Created_on) VALUES
	('1', '1', '1', 'NOW()'),
	('2', '2', '1', 'NOW()'),
	('3', '1', '1', 'NOW()'),
	('3', '2', '1', 'NOW()');
	
INSERT INTO t_doctor_office_treatment (doctor_treatment_id, doctor_office_id, Created_by, Created_on) VALUES
	('1', '1', 1, 'NOW()'),
	('2', '2', 1, 'NOW()'),
	('3', '3', 1, 'NOW()'),
	('4', '3', 1, 'NOW()');

INSERT INTO t_doctor_treatment (doctor_id, name, Created_by, Created_on) VALUES
	('1', 'Medical Checkup', 1, 'NOW()'),
	('2', 'Medical Checkup', 1, 'NOW()'),
	('3', 'Medical Checkup', 1, 'NOW()'),
	('3', 'Resep dokter', 1, 'NOW()');
	
INSERT INTO m_bank (name,va_code,created_by,created_on)
values
('mandiri','1','1',now()),
('bca','2','1',now());

insert into role(name,code,created_by,created_on,deleted_by,modified_by)
values
('Admin','Admin_Role','1',now(),0,0),
('Dokter','Dokter_Role','1',now(),0,0),
('Pasien','Pasien_Role','1',now(),0,0),
('Faskes','Faskes_Role','1',now(),0,0);

insert into m_user
(biodata_id,role_id,email,password,login_attempt,created_by,created_on)
values
(1,1,'stevenalbertus80@gmail.com','123456',0,'1',now()),
(2,2,'stevenalbertus81@gmail.com','90876532',0,'1',now()),
(3,3,'coba@gmail.com','123',0,'1',now());


insert into token
(email,user_id,token,expired,is_expired,used_for,created_By,created_On, modified_By, modified_On,deleted_By, deleted_On,is_Delete)
values
('stevenalbertus80@gmail.com',1,'123',now(),false,'asem',1,now(),1,now(),1,now(),false);

insert into m_menu(name,url,parent_id,create_by,create_on,delete_by,modified_by) values
('User Management','/usermanagement',1,1,now(),0,0),
('Hak Akses','/hakakses',3,1,now(),0,0),
('Menu Management','/aturhakakses',3,1,now(),0,0),
('dokter','/dokter',2,1,now(),0,0),
('profile','/profile',1,1,now(),0,0);

insert into m_menu_role(menu_id,role_id,create_by,create_on,delete_by,modified_by) values
(1,1,1,now(),0,0),
(2,1,1,now(),0,0),
(3,1,1,now(),0,0),
(2,3,1,now(),0,0), 
(3,1,1,now(),0,0),
(4,2,1,now(),0,0);

insert into m_customer
(biodata_id,dob) values  
(1,'1999-01-22'),
(2,'1998-01-21');


insert into m_biodata_address 
(biodata_id,label,recipient,recipient_phone_number,location_id,postal_code,address,created_by,created_on,modified_by,deleted_by)
values 
(1,'Rumah1','Juliantoroo','081284478711',2,'46578','Jl.Anyer Panarukan',2,now(),0,0),
(2,'Rumah2','Juliantorow','081284478712',3,'46578','Jl.Anyer Panarukan',2,now(),0,0),
(3,'Rumah3','Juliantoroq','081284478713',4,'46578','Jl.Anyer Panarukan',2,now(),0,0);