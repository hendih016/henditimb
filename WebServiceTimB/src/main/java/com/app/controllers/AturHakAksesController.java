package com.app.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class AturHakAksesController {
	@GetMapping("aturhakakses")
	public String aturhakakses() {
		return "aturhakakses/aturhakakses";
	}
	
	@GetMapping("addaturhakakses")
	public String addhakakses() {
		return "aturhakakses/add";
	}
	
	@GetMapping("updateaturhakakses")
	public String updatehakakses() {
		return "aturhakakses/update";
	}
	

	@GetMapping("deleteaturhakakes")
	public String deletehakakses() {
		return "aturhakakses/delete";
	}


}
