package com.app.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class MenuController {
	@GetMapping("menu")
	public String menu() {
		return "menu/menu";
	}
	
	@GetMapping("addmenu")
	public String addmenu() {
		return "menu/addmenu";
	}
	
	@GetMapping("updatemenu")
	public String updatemenu() {
		return "menu/update";
	}
	

	@GetMapping("deletemenu")
	public String deletemenu() {
		return "menu/delete";
	}

}
