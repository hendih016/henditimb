package com.app.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class HubunganCtrl {
	@GetMapping("hubungan")
	public String listhubungan() {
		return "hubungan/hubungan";
	}
	
	@GetMapping("addhubungan")
	public String addhubungan() {
		return "hubungan/addhubungan";
	}
	
	@GetMapping("edithubungan")
	public String edithubungan() {
		return "hubungan/edithubungan";
	}
}
