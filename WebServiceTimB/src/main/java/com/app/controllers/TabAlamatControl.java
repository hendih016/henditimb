package com.app.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class TabAlamatControl {

	@GetMapping("viewtabalamat")
	public String viewTabAlamat() {
		return "/tabalamat/tabalamat";
	}
	
	@GetMapping("addalamat")
	public String addalamat() {
		return "/tabalamat/add";
	}
	
	@GetMapping("updatealamat")
	public String updatealamat() {
		return "/tabalamat/update";
	}
	
	@GetMapping("deletealamat")
	public String deletealamat() {
		return "/tabalamat/delete";
	}
}
