package com.app.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class CariDoktorControl {
	@GetMapping("dummy")
	public String dummy() {
		return "caridokter/dummydokter.html";
	}

	@GetMapping("caridokter")
	public String cariDokter() {
		return "caridokter/caridokter.html";
	}
}
