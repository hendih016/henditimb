$(document).ready(function() {
	$("#kodecheck").hide();

	$("#btnsave").click(function() {
		let kode = $("#iKode").val()

		if (kode == "" || kode.length == 0 || kode == undefined) {
			$("#kodecheck").show();
			return false
		}

		else {
			$.ajax({
				url: "http://localhost:81/api/blood/listbyname/" + kode,
				type: "GET",
				contentType: "application/json",
				success: function(hasil) {
					if (hasil.length != 0) {
						alert("data sudah ada")
					}
					else {
						$("#kodecheck").hide();

						var obj = {}
						
						obj.code = $("#iKode").val().toUpperCase()
						obj.description = $("#iDes").val()
						
						var myJson = JSON.stringify(obj)
						$.ajax({
							url: "http://localhost:81/api/blood/addblood",
							type: "POST",
							contentType: "application/json",
							data: myJson,
							success: function(hasil) {
								swal({
									title: "Succes?",
									text: "Data berhasil ditambahkan",
									icon: "success",
									buttons: true,
								})
								$("#modalBlood").modal('hide')
								kembali()
							}
						})
					}

				}

			})

		}

	})

	function kembali() {
		$.ajax({
			url: "http://localhost/blood",
			type: "GET",
			dataType: "html",
			success: function(hasil) {
				console.log(hasil)
				$(".isimain").html(hasil)
			}
		})
	}
})