$(document).ready(function() {

	apibaseserver = "http://localhost:81/"
	apibaseurl = "api/lokasi/"

	$("#btnAdd").click(function() {
		$.ajax({
			url: "http://localhost/lokasiadd",
			type: "GET",
			dataType: "html",
			success: function(hasil) {
				$("#modalVariants").modal('show')
				$(".isiModalVariants").html(hasil)
			}
		})
		return false
	})

	$("#btnSearch").click(function() {
		list($("#txtCari").val())
	})

	list()

	function list(val) {

		let listurl = apibaseserver + apibaseurl + "list"

		if (val != undefined && val != "") {
			alert(val)
			listurl = listurl + "/" + val
		}

		$.ajax({
			url: listurl,
			type: "GET",
			success: function(hasil) {
				
				let txt = ""
				for (i = 0; i < hasil.length; i++) {
					txt += "<tr>"
					txt += "<td>" + hasil[i].namadaerah + "</td>"
					txt += "<td>" + hasil[i].namadaerahatas + "</td>"
					txt += "<td> <input type='button' value='Edit' class='btnEdit btn btn-primary' name='" + hasil[i].id + "'> "
					txt += "<input type='button' value='Delete' class='btnDelete btn btn-danger' name='" + hasil[i].id + "'> </td>"
					txt += "</tr>"
				}

				$('#tbll').empty();
				$('#tbll').append(txt);

				$('#myTable').DataTable({
					searching: false,
					info: false,
					retrieve: true,

					columns: [
						{ title: 'Nama Daerah' },
						{ title: 'Nama Daerah Diatasnya' },
						{ title: '#' },
					]
				});

				/*$("#tbll").empty();
				$("#tbll").append(txt)*/

				$(".btnEdit").click(function() {
					let id = $(this).attr("name");
					sessionStorage.setItem("idL", id);
					$.ajax({
						url: "http://localhost/lokasiedit",
						type: "GET",
						dataType: "html",
						success: function(hasil) {
							$("#modalVariants").modal('show')
							$(".isiModalVariants").html(hasil)
						}
					})
					return false
				})

				$(".btnDelete").click(function() {
					let id = $(this).attr("name");

					$.ajax({
						url: apibaseserver + apibaseurl + "listByID/" + id,
						type: "GET",
						success: function(hasil) {
							var obj = {}
/*							obj.id = hasil.id
							obj.name = hasil.name
							obj.parentId = hasil.parentId
							obj.locationLevelId = hasil.locationLevelId
							obj.c_by = hasil.c_by
							obj.c_on = hasil.c_on
							obj.m_by = hasil.m_by
							obj.m_on = hasil.m_on
							obj.d_by = 3
							obj.d_on = hasil.d_on
							obj.is_del = true*/
							var myJson = JSON.stringify(obj)

							console.log(myJson)

							if (confirm("Apa anda yakin?")) {
								$.ajax({
									url: apibaseserver + apibaseurl + "update",
									type: "PUT",
									contentType: "application/json",
									data: myJson,
									success: function(hasil) {
										alert("Hapus berhasil")

										list()
									}
								})
							}
							return false;
						}
					})
				})
			}
		})
	}
})
