$(document).ready(function() {
	$("#namecheck").hide();
	BloodById()

	function BloodById() {
		let id = sessionStorage.getItem("id")
		$.ajax({
			url: "http://localhost:81/api/relation/list/" + id,
			type: "GET",
			success: function(hasil) {
				$("#iId").val(hasil.id)
				$("#iNama").val(hasil.name)
			}
		})
	}

	$("#btnsave").click(function() {
		let id = sessionStorage.getItem("id")
		let name = $("#iNama").val()

		if (name == "" || name.length == 0 || name == undefined) {
			$("#namecheck").show();
			return false
		}

		else {
			$.ajax({
				url: "http://localhost:81/api/relation/listbyname/" + name,
				type: "GET",
				contentType: "application/json",
				success: function(hasil) {
					if (hasil.length != 0) {
						alert("data sudah ada")
					}
					else {
						var obj = {}
						obj.id = id
						obj.name = $("#iNama").val()

						var myJson = JSON.stringify(obj)

						$.ajax({

							url: "http://localhost:81/api/relation/editcr",
							type: "PUT",
							contentType: "application/json",
							data: myJson,
							success: function(hasil) {
								swal({
									title: "Succes?",
									text: "Data berhasil di edit",
									icon: "success",
									buttons: true,
								})
								$("#modalHubungan").modal('hide')
								kembali()
							}
						})
					}

				}


			})
		}
	})

	function kembali() {
		$.ajax({
			url: "http://localhost/hubungan",
			type: "GET",
			dataType: "html",
			success: function(hasil) {
				$(".isimain").html(hasil)
			}
		})
	}

})