$(document).ready(function() {
	$("#namecheck").hide();

	$("#btnsave").click(function() {
		let name = $("#iNama").val()

		if (name == "" || name.length == 0 || name == undefined) {
			$("#namecheck").show();
			return false
		}
		else {
			$.ajax({
				url: "http://localhost:81/api/relation/listbyname/" + name,
				type: "GET",
				contentType: "application/json",
				success: function(hasil) {
					if (hasil.length != 0) {
						alert("data sudah ada")
					}
					else {
						$("#namecheck").hide();
						var obj = {}
						obj.name = $("#iNama").val()
						var myJson = JSON.stringify(obj)
						$.ajax({
							url: "http://localhost:81/api/relation/addcr",
							type: "POST",
							contentType: "application/json",
							data: myJson,
							success: function(hasil) {
								swal({
									title: "Succes?",
									text: "Data berhasil ditambahkan",
									icon: "success",
									buttons: true,
								})
								$("#modalHubungan").modal('hide')
								kembali();
							}
						})

					}

				}

			})

		}

	})

	function kembali() {
		$.ajax({
			url: "http://localhost/hubungan",
			type: "GET",
			dataType: "html",
			success: function(hasil) {
				$(".isimain").html(hasil)
			}
		})
	}
})